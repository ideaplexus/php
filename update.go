package main

import (
	"fmt"
	"github.com/charmbracelet/log"
	yaml "github.com/goccy/go-yaml"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"
)

type Target struct {
	Name   string `yaml:"name"`
	Parent string `yaml:"parent"`
}

type Variant struct {
	Version string   `yaml:"version"`
	Target  []Target `yaml:"target"`
	GpgKeys string   `yaml:"gpgKeys"`
	AscUrl  string   `yaml:"ascUrl"`
	Url     string   `yaml:"url"`
	Sha256  string   `yaml:"sha256"`
}

type Config struct {
	Variant []Variant           `yaml:"variant"`
	Files   map[string][]string `yaml:"files"`
}

type Updater struct {
	config *Config `yaml:"config"`
}

type DockerTemplateData struct {
	VersionMajorMinor string
	Version           string
	GpgKeys           string
	AscUrl            string
	Url               string
	Sha256            string
	Target            string
	AdditionalVariant string
	Parent            string
}

type GitlabVariant struct {
	VersionMajorMinor string
	BuildPath         string
	BuildTargets      []Target
	FinalTargets      []Target
}

type GitlabTemplateData struct {
	Stages             []string
	Latest             string
	Variants           []GitlabVariant
	AdditionalVariants []string
}

func main() {
	updater := NewUpdater()

	updater.render()
}

func NewUpdater() *Updater {
	return &Updater{
		config: loadConfig(),
	}
}

func loadConfig() *Config {
	file, err := os.ReadFile("config.yaml")
	handleError(err)

	config := Config{}
	handleError(yaml.Unmarshal(file, &config))

	return &config
}

const dist = "./dist"
const src = "./src"

const (
	DefaultFilePerm = 0o644
	DefaultDirPerm  = 0o755
)

var phpVersionRegex = regexp.MustCompile(`(\d\.\d)\.\d+`)

func (u *Updater) render() {
	for _, variant := range u.config.Variant {
		_ = u.renderDockerfileBasic(variant)

		for _, target := range variant.Target {
			_ = u.renderDockerfileBuild(variant, target)
			_ = u.renderDockerfileAdditional(variant, target)
			_ = u.renderDockerfileCombined(variant, target)
		}
	}

	u.renderGitlabCIFile()
}

func versionMajorMinor(version string) string {
	vMM := ""
	match := phpVersionRegex.FindStringSubmatch(version)
	if len(match) == 2 {
		vMM = match[1]
	}

	return vMM
}

func (u *Updater) renderDockerfileBasic(variant Variant) error {
	version := versionMajorMinor(variant.Version)

	path := dist + "/" + version

	err := os.MkdirAll(path, DefaultDirPerm)
	if err != nil {
		return err
	}

	data := &DockerTemplateData{
		VersionMajorMinor: version,
		Version:           variant.Version,
		GpgKeys:           variant.GpgKeys,
		AscUrl:            variant.AscUrl,
		Url:               variant.Url,
		Sha256:            variant.Sha256,
		Target:            "",
		AdditionalVariant: "",
		Parent:            "",
	}

	name := "Dockerfile.basic.tmpl"

	basic, err := os.OpenFile(path+"/Dockerfile.basic", os.O_RDWR|os.O_CREATE|os.O_TRUNC, DefaultFilePerm)
	if err != nil {
		return fmt.Errorf("failed to open target file: %w", err)
	}
	defer handleClose(basic)

	tmpl := template.Must(template.New(name).ParseFiles(fmt.Sprintf("./src/templates/%s", name)))
	err = tmpl.Execute(basic, data)
	if err != nil {
		return err
	}

	return nil
}

func (u *Updater) renderDockerfileBuild(variant Variant, target Target) error {
	if target.Parent != "" {
		return nil
	}

	version := versionMajorMinor(variant.Version)

	data := &DockerTemplateData{
		VersionMajorMinor: version,
		Version:           variant.Version,
		GpgKeys:           variant.GpgKeys,
		AscUrl:            variant.AscUrl,
		Url:               variant.Url,
		Sha256:            variant.Sha256,
		Target:            target.Name,
		AdditionalVariant: "",
		Parent:            target.Parent,
	}

	path := dist + "/" + version + "/" + target.Name

	err := os.MkdirAll(path, DefaultDirPerm)
	if err != nil {
		return err
	}

	handleError(copyOverride(src+"/files/docker-php", path))
	handleError(copyOverride(src+"/files/extensions", path+"/extensions"))
	handleError(copyOverride(src+"/files/tools", path+"/tools"))

	name := "Dockerfile.build.tmpl"

	build, err := os.OpenFile(path+"/"+strings.TrimSuffix(filepath.Base(name), filepath.Ext(name)), os.O_RDWR|os.O_CREATE|os.O_TRUNC, DefaultFilePerm)
	if err != nil {
		return fmt.Errorf("failed to open target file: %w", err)
	}
	defer handleClose(build)

	tmpl := template.Must(template.New(name).ParseFiles(fmt.Sprintf("./src/templates/%s", name)))
	err = tmpl.Execute(build, data)
	if err != nil {
		return err
	}

	return nil
}

func (u *Updater) renderDockerfileAdditional(variant Variant, target Target) error {
	if target.Parent != "" {
		return nil
	}

	version := versionMajorMinor(variant.Version)

	data := &DockerTemplateData{
		VersionMajorMinor: version,
		Version:           variant.Version,
		GpgKeys:           variant.GpgKeys,
		AscUrl:            variant.AscUrl,
		Url:               variant.Url,
		Sha256:            variant.Sha256,
		Target:            target.Name,
		AdditionalVariant: "",
		Parent:            target.Parent,
	}

	path := dist + "/" + version + "/" + target.Name

	err := os.MkdirAll(path, DefaultDirPerm)
	if err != nil {
		return err
	}

	name := "Dockerfile.additional.tmpl"

	additionals := []string{"std", "full"}

	for _, additional := range additionals {
		data.AdditionalVariant = additional

		build, err := os.OpenFile(path+"/"+strings.TrimSuffix(filepath.Base(name), filepath.Ext(name))+"-"+additional, os.O_RDWR|os.O_CREATE|os.O_TRUNC, DefaultFilePerm)
		if err != nil {
			return fmt.Errorf("failed to open target file: %w", err)
		}
		defer build.Close()

		tmpl := template.Must(template.New(name).ParseFiles(fmt.Sprintf("./src/templates/%s", name)))
		err = tmpl.Execute(build, data)
		if err != nil {
			return err
		}

	}
	return nil
}

func (u *Updater) renderDockerfileCombined(variant Variant, target Target) error {

	version := versionMajorMinor(variant.Version)

	data := &DockerTemplateData{
		VersionMajorMinor: version,
		Version:           variant.Version,
		GpgKeys:           variant.GpgKeys,
		AscUrl:            variant.AscUrl,
		Url:               variant.Url,
		Sha256:            variant.Sha256,
		Target:            target.Name,
		AdditionalVariant: "",
		Parent:            target.Parent,
	}

	if data.Parent == "" {
		data.Parent = target.Name
	}

	path := dist + "/" + version + "/" + target.Name

	err := os.MkdirAll(path, DefaultDirPerm)
	if err != nil {
		return err
	}

	if _, err := os.Stat(src + "/files/targets/" + target.Name); !os.IsNotExist(err) {
		handleError(copyOverride(src+"/files/targets/"+target.Name, path))
	}

	name := "Dockerfile.combined.tmpl"

	additionals := []string{"std", "full"}

	for _, additional := range additionals {
		data.AdditionalVariant = additional

		combined, err := os.OpenFile(path+"/"+strings.TrimSuffix(filepath.Base(name), filepath.Ext(name))+"-"+additional, os.O_RDWR|os.O_CREATE|os.O_TRUNC, DefaultFilePerm)
		if err != nil {
			return fmt.Errorf("failed to open target file: %w", err)
		}
		defer handleClose(combined)

		tmpl := template.Must(template.New(name).ParseFiles(fmt.Sprintf("./src/templates/%s", name)))
		err = tmpl.Execute(combined, data)
		if err != nil {
			return err
		}

	}
	return nil
}

func (u *Updater) renderGitlabCIFile() error {
	name := ".gitlab-ci.yml.tmpl"

	file, err := os.OpenFile("./"+strings.TrimSuffix(filepath.Base(name), filepath.Ext(name)), os.O_RDWR|os.O_CREATE|os.O_TRUNC, DefaultFilePerm)
	if err != nil {
		return fmt.Errorf("failed to open target file: %w", err)
	}
	defer handleClose(file)

	tmpl := template.Must(template.New(name).ParseFiles(fmt.Sprintf("./src/templates/%s", name)))

	var stages []string
	var gitlabVariants []GitlabVariant
	for _, variant := range u.config.Variant {
		version := versionMajorMinor(variant.Version)

		stages = append(stages, version)

		gitlabVariant := GitlabVariant{
			VersionMajorMinor: version,
			BuildPath:         dist + "/" + version,
			BuildTargets:      []Target{},
			FinalTargets:      []Target{},
		}

		for _, target := range variant.Target {
			if target.Parent == "" {
				gitlabVariant.BuildTargets = append(gitlabVariant.BuildTargets, target)
			}
			gitlabVariant.FinalTargets = append(gitlabVariant.FinalTargets, target)
		}

		gitlabVariants = append(gitlabVariants, gitlabVariant)
	}

	data := &GitlabTemplateData{
		Stages:             stages,
		Latest:             stages[0],
		Variants:           gitlabVariants,
		AdditionalVariants: []string{"std", "full"},
	}

	err = tmpl.Execute(file, data)
	if err != nil {
		return err
	}

	return nil
}

func copyOverride(src string, dest string) error {
	entries, err := os.ReadDir(src)
	if err != nil {
		return err
	}

	if err = os.MkdirAll(dest, DefaultDirPerm); err != nil {
		return err
	}

	for _, entry := range entries {
		if entry.IsDir() {
			if err = copyOverride(src+"/"+entry.Name(), dest+"/"+entry.Name()); err != nil {
				return err
			}
		}

		destFile, err := os.OpenFile(dest+"/"+entry.Name(), os.O_RDWR|os.O_CREATE|os.O_TRUNC, DefaultFilePerm)
		if err != nil {
			return err
		}
		defer handleClose(destFile)

		srcFile, err := os.Open(src + "/" + entry.Name())
		if err != nil {
			return err
		}
		defer handleClose(srcFile)

		_, err = io.Copy(destFile, srcFile)
		if err != nil {
			return err
		}
	}

	return nil
}

func handleError(err error) {
	if err != nil {
		log.Error(err.Error())
		os.Exit(1)
	}
}

func handleClose(file *os.File) {
	handleError(file.Close())
}
