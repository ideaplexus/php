ARG BUILD_CACHE_PREFIX

FROM ${BUILD_CACHE_PREFIX}php-build-cli:8.1

COPY extensions/* /usr/local/bin/


# sodium was built as a shared module (so that it can be replaced later if so desired), so let's enable it too (https://github.com/docker-library/php/issues/598)
RUN docker-php-ext-enable sodium

COPY tools/* /usr/local/bin/

# ensure all tools are installed
RUN set -eux; \
  COMPOSER_ALLOW_SUPERUSER=1 composer --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpunit --version; \
  COMPOSER_ALLOW_SUPERUSER=1 ppm --version; \
  COMPOSER_ALLOW_SUPERUSER=1 psalm --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpstan --version; \
  COMPOSER_ALLOW_SUPERUSER=1 PHP_CS_FIXER_IGNORE_ENV=1 php-cs-fixer --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpcs --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpcbf --version; \
  COMPOSER_ALLOW_SUPERUSER=1 phpmd --version; \
  COMPOSER_ALLOW_SUPERUSER=1 box --version