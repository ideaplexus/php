ARG BUILD_CACHE_PREFIX



FROM ${BUILD_CACHE_PREFIX}php-nginx:alpine3.20 AS nginx
FROM ${BUILD_CACHE_PREFIX}php-additional-fpm-full:8.3
COPY index.php /var/www/html/public/index.php
COPY --from=nginx /etc/nginx /etc/nginx
COPY --from=nginx /usr/sbin/nginx* /usr/sbin/
COPY --from=nginx /usr/share/nginx /usr/share/nginx
COPY --from=nginx /usr/lib/nginx/modules /usr/lib/nginx/modules

COPY supervisord.conf /etc/supervisord.conf
COPY supervisord-php-fpm.conf /etc/supervisor/conf.d/php-fpm.conf
COPY supervisord-nginx.conf /etc/supervisor/conf.d/nginx.conf
COPY nginx.conf /etc/nginx/nginx.conf
COPY default.conf /etc/nginx/conf.d/default.conf

RUN set -eux; \
    mkdir -p /var/cache/nginx; \
    mkdir -p /var/log/nginx; \
    chown 82:82 /var/cache/nginx; \
    chown 82:82 /var/log/nginx; \
    NGINX_RUN_DEPS="$( \
    scanelf --needed --nobanner --format '%n#p' /usr/sbin/nginx /usr/lib/nginx/modules/*.so \
        | tr ',' '\n' \
        | sort -u \
        | awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
    )"; \
    apk add --no-cache \
        supervisor \
        $NGINX_RUN_DEPS

ENTRYPOINT ["docker-php-entrypoint"]


WORKDIR /var/www/html

RUN set -eux; \
    cd /usr/local/etc; \
    if [ -d php-fpm.d ]; then \
        # for some reason, upstream's php-fpm.conf.default has "include=NONE/etc/php-fpm.d/*.conf"
        sed 's!=NONE/!=!g' php-fpm.conf.default | tee php-fpm.conf > /dev/null; \
        cp php-fpm.d/www.conf.default php-fpm.d/www.conf; \
    else \
        # PHP 5.x doesn't use "include=" by default, so we'll create our own simple config that mimics PHP 7+ for consistency
        mkdir php-fpm.d; \
        cp php-fpm.conf.default php-fpm.d/www.conf; \
        { \
            echo '[global]'; \
            echo 'include=etc/php-fpm.d/*.conf'; \
        } | tee php-fpm.conf; \
    fi; \
    { \
        echo '[global]'; \
        echo 'error_log = /proc/self/fd/2'; \
        echo; echo '; https://github.com/docker-library/php/pull/725#issuecomment-443540114'; echo 'log_limit = 8192'; \
        echo; \
        echo '[www]'; \
        echo '; if we send this to /proc/self/fd/1, it never appears'; \
        echo 'access.log = /proc/self/fd/2'; \
        echo; \
        echo 'clear_env = no'; \
        echo; \
        echo '; Ensure worker stdout and stderr are sent to the main error log.'; \
        echo 'catch_workers_output = yes'; \
        echo 'decorate_workers_output = no'; \
        echo; \
        echo '; some performance optimizations'; \
        echo 'pm = dynamic'; \
        echo 'pm.max_children = 5'; \
        echo 'pm.start_servers = 2'; \
        echo 'pm.min_spare_servers = 1'; \
        echo 'pm.max_spare_servers = 3'; \
    } | tee php-fpm.d/docker.conf; \
    { \
        echo '[global]'; \
        echo 'daemonize = no'; \
        echo 'events.mechanism = epoll'; \
        echo; \
        echo '[www]'; \

        echo 'listen = /run/php-fpm.sock'; \
        echo 'listen.owner = www-data'; \
        echo 'listen.group = www-data'; \
        echo 'listen.mode = 0660'; \
    } | tee php-fpm.d/zz-docker.conf

# Override stop signal to stop process gracefully
# https://github.com/php/php-src/blob/17baa87faddc2550def3ae7314236826bc1b1398/sapi/fpm/php-fpm.8.in#L163
STOPSIGNAL SIGQUIT

EXPOSE 8080

CMD ["supervisord", "-n", "-c", "/etc/supervisord.conf"]
